# translation of kcmfonts.po to Occitan (lengadocian)
# Occitan translation of kcmfonts.po
# Occitan translation of kcmfonts.po
# Copyright (C) 2002,2003, 2004, 2006, 2007, 2008 Free Software Foundation, Inc.
#
# Ludovic Grossard <grossard@kde.org>, 2002,2003.
# Charles de Miramon <cmiramon@kde-france.org>, 2003.
# Delafond <gerard@delafond.org>, 2003, 2004.
# Matthieu Robin <kde@macolu.org>, 2004, 2006.
# Nicolas Ternisien <nicolas.ternisien@gmail.com>, 2006.
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcmfonts\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-15 00:48+0000\n"
"PO-Revision-Date: 2008-08-05 22:26+0200\n"
"Last-Translator: Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>\n"
"Language-Team: Occitan (lengadocian) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: KBabel 1.11.4\n"

#. i18n: ectx: label, entry (forceFontDPIWayland), group (General)
#: fontsaasettingsbase.kcfg:9
#, kde-format
msgid "Force font DPI Wayland"
msgstr ""

#. i18n: ectx: label, entry (forceFontDPI), group (General)
#: fontsaasettingsbase.kcfg:13
#, kde-format
msgid "Force font DPI on X11"
msgstr ""

#. i18n: ectx: label, entry (font), group (General)
#: fontssettings.kcfg:9
#, fuzzy, kde-format
#| msgid "General"
msgid "General font"
msgstr "General"

#. i18n: ectx: label, entry (fixed), group (General)
#: fontssettings.kcfg:21
#, kde-format
msgid "Fixed width font"
msgstr ""

#. i18n: ectx: label, entry (smallestReadableFont), group (General)
#: fontssettings.kcfg:33
#, fuzzy, kde-format
#| msgid "Small"
msgid "Small font"
msgstr "Pichon"

#. i18n: ectx: label, entry (toolBarFont), group (General)
#: fontssettings.kcfg:45
#, fuzzy, kde-format
#| msgid "Toolbar"
msgid "Tool bar font"
msgstr "Barra d'espleches"

#. i18n: ectx: label, entry (menuFont), group (General)
#: fontssettings.kcfg:57
#, kde-format
msgid "Menu font"
msgstr ""

#. i18n: ectx: label, entry (activeFont), group (WM)
#: fontssettings.kcfg:71
#, kde-format
msgid "Window title font"
msgstr ""

#: kxftconfig.cpp:458
#, fuzzy, kde-format
#| msgctxt "Use anti-aliasing"
#| msgid "System Settings"
msgctxt "use system subpixel setting"
msgid "Vendor default"
msgstr "Configuracion del sistèma"

#: kxftconfig.cpp:460
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no subpixel rendering"
msgid "None"
msgstr "Pas cap"

#: kxftconfig.cpp:462
#, kde-format
msgid "RGB"
msgstr "RVB"

#: kxftconfig.cpp:464
#, kde-format
msgid "BGR"
msgstr "BVR"

#: kxftconfig.cpp:466
#, kde-format
msgid "Vertical RGB"
msgstr "RVB vertical"

#: kxftconfig.cpp:468
#, kde-format
msgid "Vertical BGR"
msgstr "BVR vertical"

#: kxftconfig.cpp:496
#, fuzzy, kde-format
#| msgctxt "Use anti-aliasing"
#| msgid "System Settings"
msgctxt "use system hinting settings"
msgid "Vendor default"
msgstr "Configuracion del sistèma"

#: kxftconfig.cpp:498
#, fuzzy, kde-format
#| msgid "Medium"
msgctxt "medium hinting"
msgid "Medium"
msgstr "Mejan"

#: kxftconfig.cpp:500
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no hinting"
msgid "None"
msgstr "Pas cap"

#: kxftconfig.cpp:502
#, kde-format
msgctxt "slight hinting"
msgid "Slight"
msgstr ""

#: kxftconfig.cpp:504
#, fuzzy, kde-format
#| msgid "Full"
msgctxt "full hinting"
msgid "Full"
msgstr "Complet"

#: package/contents/ui/main.qml:21
#, kde-format
msgid "This module lets you configure the system fonts."
msgstr ""

#: package/contents/ui/main.qml:26
#, kde-format
msgid "Adjust Global Scale…"
msgstr ""

#: package/contents/ui/main.qml:37
#, kde-format
msgid ""
"Some changes such as anti-aliasing or DPI will only affect newly started "
"applications."
msgstr ""

#: package/contents/ui/main.qml:51
#, kde-format
msgid ""
"Very large fonts may produce odd-looking results. Instead of using a very "
"large font size, consider adjusting the global screen scale."
msgstr ""

#: package/contents/ui/main.qml:72
#, kde-format
msgid ""
"Decimal font sizes can cause text layout problems in some applications. "
"Consider using only integer font sizes."
msgstr ""

#: package/contents/ui/main.qml:94
#, kde-format
msgid ""
"The recommended way to scale the user interface is using the global screen "
"scaling feature."
msgstr ""

#: package/contents/ui/main.qml:106
#, kde-format
msgid "&Adjust All Fonts…"
msgstr ""

#: package/contents/ui/main.qml:119
#, fuzzy, kde-format
#| msgid "General"
msgid "General:"
msgstr "General"

#: package/contents/ui/main.qml:120
#, fuzzy, kde-format
#| msgid "General"
msgid "Select general font"
msgstr "General"

#: package/contents/ui/main.qml:131
#, kde-format
msgid "Fixed width:"
msgstr ""

#: package/contents/ui/main.qml:132
#, kde-format
msgid "Select fixed width font"
msgstr ""

#: package/contents/ui/main.qml:143
#, fuzzy, kde-format
#| msgid "Small"
msgid "Small:"
msgstr "Pichon"

#: package/contents/ui/main.qml:144
#, fuzzy, kde-format
#| msgid "Small"
msgid "Select small font"
msgstr "Pichon"

#: package/contents/ui/main.qml:155
#, fuzzy, kde-format
#| msgid "Toolbar"
msgid "Toolbar:"
msgstr "Barra d'espleches"

#: package/contents/ui/main.qml:156
#, fuzzy, kde-format
#| msgid "Toolbar"
msgid "Select toolbar font"
msgstr "Barra d'espleches"

#: package/contents/ui/main.qml:167
#, fuzzy, kde-format
#| msgid "Menu"
msgid "Menu:"
msgstr "Menut"

#: package/contents/ui/main.qml:168
#, kde-format
msgid "Select menu font"
msgstr ""

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Window title:"
msgstr ""

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Select window title font"
msgstr ""

#: package/contents/ui/main.qml:194
#, kde-format
msgid "Anti-Aliasing:"
msgstr ""

#: package/contents/ui/main.qml:199
#, fuzzy, kde-format
#| msgctxt "Use anti-aliasing"
#| msgid "Enabled"
msgid "Enable"
msgstr "Activat"

#: package/contents/ui/main.qml:203
#, kde-kuit-format
msgctxt "@info:tooltip Anti-Aliasing"
msgid ""
"Pixels on displays are generally aligned in a grid. Therefore shapes of "
"fonts that do not align with this grid will look blocky and wrong unless "
"<emphasis>anti-aliasing</emphasis> techniques are used to reduce this "
"effect. You generally want to keep this option enabled unless it causes "
"problems."
msgstr ""

#: package/contents/ui/main.qml:217
#, kde-format
msgid "Exclude range from anti-aliasing"
msgstr ""

#: package/contents/ui/main.qml:236 package/contents/ui/main.qml:259
#, kde-format
msgid "%1 pt"
msgstr ""

#: package/contents/ui/main.qml:251
#, fuzzy, kde-format
#| msgid " to "
msgid "to"
msgstr " fins a "

#: package/contents/ui/main.qml:282
#, kde-format
msgctxt "Used as a noun, and precedes a combobox full of options"
msgid "Sub-pixel rendering:"
msgstr ""

#: package/contents/ui/main.qml:324
#, kde-kuit-format
msgctxt "@info:tooltip Sub-pixel rendering"
msgid ""
"<para>On TFT or LCD screens every single pixel is actually composed of three "
"or four smaller monochrome lights. These <emphasis>sub-pixels</emphasis> can "
"be changed independently to further improve the quality of displayed fonts.</"
"para> <para>The rendering quality is only improved if the selection matches "
"the manner in which the sub-pixels of your display are aligned. Most "
"displays have a linear ordering of <emphasis>RGB</emphasis> sub-pixels, some "
"have <emphasis>BGR</emphasis> and some exotic orderings are not supported by "
"this feature.</para>This does not work with CRT monitors."
msgstr ""

#: package/contents/ui/main.qml:329
#, kde-format
msgctxt "Used as a noun, and precedes a combobox full of options"
msgid "Hinting:"
msgstr ""

#: package/contents/ui/main.qml:370
#, kde-kuit-format
msgctxt "@info:tooltip Hinting"
msgid ""
"Hinting is a technique in which hints embedded in a font are used to enhance "
"the rendering quality especially at small sizes. Stronger hinting generally "
"leads to sharper edges but the small letters will less closely resemble "
"their shape at big sizes."
msgstr ""

#: package/contents/ui/main.qml:380
#, kde-format
msgid "Force font DPI:"
msgstr ""

#: package/contents/ui/main.qml:422
#, kde-kuit-format
msgctxt "@info:tooltip Force fonts DPI"
msgid ""
"<para>Enter your screen's DPI here to make on-screen fonts match their "
"physical sizes when printed. Changing this option from its default value "
"will conflict with many apps; some icons and images may not scale as "
"expected.</para><para>To increase text size, change the size of the fonts "
"above. To scale everything, use the scaling slider on the <interface>Display "
"& Monitor</interface> page.</para>"
msgstr ""

#: package/contents/ui/main.qml:428
#, kde-format
msgid "Select Font"
msgstr ""

#, fuzzy
#~| msgctxt "Use anti-aliasing"
#~| msgid "System Settings"
#~ msgid "Vendor default"
#~ msgstr "Configuracion del sistèma"

#, fuzzy
#~| msgctxt "Use anti-aliasing"
#~| msgid "Disabled"
#~ msgid "Disabled"
#~ msgstr "Desactivat"

#, fuzzy
#~| msgid "Configure..."
#~ msgid "Configure the system fonts"
#~ msgstr "Configurar..."

#~ msgctxt "Use anti-aliasing"
#~ msgid "System Settings"
#~ msgstr "Configuracion del sistèma"

#, fuzzy
#~| msgid "Desktop"
#~ msgctxt "font usage"
#~ msgid "Desktop"
#~ msgstr "Burèu"

#~ msgctxt "Force fonts DPI"
#~ msgid "Disabled"
#~ msgstr "Desactivat"

#~ msgid "96 DPI"
#~ msgstr "96 DPI"
